export default class action {
  constructor(type = 'default', payload) {
    this.type = type;
    this.payload = payload || undefined;
  }
}
