import dispatch from "./dispatchers";
import action from "./actions";
import { getState } from "./services";

function render() {
  const data = getState();
  const count = document.querySelector("[data-count]");
  count.textContent = data.count;
}

function increaseButtonHandler() {
  const increase = document.querySelector("[data-increase]");
  increase.addEventListener("click", () => {
    dispatch(new action("increase"));
  });
}

function decreaseButtonHandler() {
  const decrease = document.querySelector("[data-decrease]");
  decrease.addEventListener("click", () => {
    dispatch(new action("decrease"));
  });
}

export function rerender() {
  const app = document.querySelector("#app");
  app.dispatchEvent(renderEvent);
}

const renderEvent = new Event("render");

export function init() {
  const app = document.querySelector("#app");
  app.addEventListener("render", render);
  increaseButtonHandler();
  decreaseButtonHandler();
  app.dispatchEvent(renderEvent);
}
