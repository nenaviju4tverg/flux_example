import { increaseState, decreaseState } from "./services";

export default function (action) {
  switch (action.type) {
    case "increase":
      increaseState();
      break;
    case "decrease":
      decreaseState();
      break;
    case "default":
      break;
  }
}
