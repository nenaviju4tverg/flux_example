import { state } from "./store";
import { rerender } from "./controllers";

export function getState() {
  return state;
}

export function increaseState() {
  state.count += 1;
  rerender();
}

export function decreaseState() {
  state.count -= 1;
  rerender();
}
